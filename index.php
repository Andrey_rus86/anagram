/**
 * Подсчет уникальных символов
 * @param $str - слово
 * @return array массив, в котором ключи - символы, значение - их количество в строке
 */
function countUniqueSymbols($str) {
    $symbols = [];
    $len = mb_strlen($str, 'UTF-8');
    for($i=0; $i < $len; $i++) {
        $letter = mb_substr($str,$i,1, 'UTF-8');
        if(!in_array($letter, array_keys($symbols))) {
            $symbols[mb_strtolower($letter)] = 1;
        } else {
            $symbols[$letter]++;
        }
    }

    return $symbols;
}

$word1 = 'Колба';
$word2 = 'бокал';

if(countUniqueSymbols($word1) == countUniqueSymbols($word2)) {
    echo "Words $word1 and $word2 are anagrams";
} else {
    echo "Words $word1 and $word2 are NOT anagrams";
}